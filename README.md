# 3ISP11-20Test



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- <b>[ERD- диаграмма] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/ER-Diogramm.png)</b>
<b>![ER-Diogramm](./ER-Diogramm.png)</b>

-<b>[Use-case] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/UseCase.png)</b>
<b>![UseCase](./UseCase.png)</b>

-<b>[Диагамма-последовательности] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%94%D0%B8%D0%B0%D0%B3%D0%B0%D0%BC%D0%BC%D0%B0-_%D0%9F%D0%BE%D1%81%D0%BB%D0%B5%D0%B4%D0%BE%D0%B2%D0%B0%D1%82%D0%B5%D0%BB%D1%8C%D0%BD%D0%BE%D1%81%D1%82%D0%B8.png)</b>
<b>![Диагамма-последовательности](./Диагамма-_Последовательности.png)</b>

-<b>[База данных] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%91%D0%B0%D0%B7%D0%B0_%D0%B4%D0%B0%D0%BD%D0%BD%D1%8B%D1%85.png)</b>
<b>![База данных](./База_данных.png)</b>

-<b>[Авторизация] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%90%D0%B2%D1%82%D0%BE%D1%80%D0%B8%D0%B7%D0%B0%D1%86%D0%B8%D1%8F.png)</b>
<b>![Авторизация](./Авторизация.png)</b>

-<b>[Главное окно] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%93%D0%BB%D0%B0%D0%B2%D0%BD%D0%BE%D0%B5_%D0%BE%D0%BA%D0%BD%D0%BE.png)</b>
<b>![Главное окно](./Главное_окно.png)</b>

-<b>[Новый заказ] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%9D%D0%BE%D0%B2%D1%8B%D0%B9_%D0%B7%D0%B0%D0%BA%D0%B0%D0%B7.png)</b>
<b>![Новый заказ](./Новый_заказ.png)</b>

-<b>[Продукты] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%9F%D1%80%D0%BE%D0%B4%D1%83%D0%BA%D1%82%D1%8B.png)</b>
<b>![Продукты](./Продукты.png)</b>

-<b>[Все окна] (https://gitlab.com/3isp1120/3isp11-20test/-/blob/main/%D0%92%D1%81%D0%B5_%D0%BE%D0%BA%D0%BD%D0%B0.png)</b>
<b>![Все_окна](./Все_окна.png)</b>


