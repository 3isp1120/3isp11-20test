﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magaz.Classes
{
    internal class OrderCart
    {
        public static List<DB.VW_ProductList> orderCart { get; set; } = new List<DB.VW_ProductList>();
        public static DB.Staff cartStaff { get; set; } = new DB.Staff();
    }
}
