﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Magaz.Windows
{
    /// <summary>
    /// Логика взаимодействия для AuthWindow.xaml
    /// </summary>
    public partial class AuthWindow : Window
    {
        DB.Staff savedEmployee = null;

        public AuthWindow()
        {
            InitializeComponent();
        }

        private void BtnEnter_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(TbPassword.Text) && !string.IsNullOrEmpty(TbLogin.Text))
            {
                savedEmployee = Classes.EF.Context.Staff.ToList().Where(i => i.Password == TbPassword.Text && i.Login == TbLogin.Text).FirstOrDefault();

                Classes.SavedUser.SavedEmployee = savedEmployee;

                if (savedEmployee != null)
                {
                    MainMenu mainMenu = new MainMenu();
                    this.Close();
                    mainMenu.Show();

                }
                else
                {
                    MessageBox.Show("Данные неверны или их нет", "Авторизация", MessageBoxButton.OK, MessageBoxImage.Stop);
                }
            }
        }
    }
}
