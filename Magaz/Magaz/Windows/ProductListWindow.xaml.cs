﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Magaz.Windows
{
    /// <summary>
    /// Логика взаимодействия для ProductListWindow.xaml
    /// </summary>
    public partial class ProductListWindow : Window
    {
        public ProductListWindow()
        {
            InitializeComponent();
            LVProductList.ItemsSource = Classes.EF.Context.VW_ProductList.ToList();
        }

        private void BtnError_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null) 
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;


            if (product.AmountInStock >= 1)
            {
                Classes.OrderCart.orderCart.Add(product);
                switch (product.MeasureUnit)
                {
                    case "единица товара":
                        if (product.Quantity < 99)
                        {
                            product.Quantity++;
                        }
                        break;
                    default:
                        if (product.Quantity < 99)
                        {
                            product.Quantity += (decimal)0.1;
                        }
                        break;
                }
            }
            else
            {
                MessageBox.Show("Товар недоступен");
                return;
            }

            NewOrderWindow newOrderWindow = new NewOrderWindow();
            newOrderWindow.Show();
            this.Close();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            NewOrderWindow newOrderWindow = new NewOrderWindow();
            newOrderWindow.Show();
            this.Close();
        }
    }
}
