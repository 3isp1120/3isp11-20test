﻿using Magaz.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Magaz.Windows
{
    /// <summary>
    /// Логика взаимодействия для NewOrderWindow.xaml
    /// </summary>
    public partial class NewOrderWindow : Window
    {
        public int DeliveryFee = 0;

        public NewOrderWindow()
        {
            InitializeComponent();
            SetListValues();

            CmbDeliveryType.ItemsSource = Classes.EF.Context.DeliveryType.ToList();
            CmbDeliveryType.DisplayMemberPath = "Title";
        }

        public void SetListValues() 
        {
            ObservableCollection<DB.VW_ProductList> listCart = new ObservableCollection<DB.VW_ProductList>(Classes.OrderCart.orderCart);
            LVMiniProductList.ItemsSource = listCart.Distinct();
            decimal totalSum = 0;
            foreach (var item in listCart)
            {
                totalSum += item.Price * item.Quantity;
            }
            TbTotalSum.Text = totalSum.ToString() + " ₽";
        }

        private void BtnAddItemToOrder_Click(object sender, RoutedEventArgs e)
        {
            ProductListWindow productList = new ProductListWindow();
            productList.Show();
            this.Close();
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainMenu main = new MainMenu();
            if (Classes.OrderCart.orderCart.Count() != 0)
            {
                switch (MessageBox.Show("Вы уверены, что хотите закрыть заказ? Имеющийся заказ будет удален", "Закрыть окно", MessageBoxButton.YesNo, MessageBoxImage.Exclamation))
                {
                    case MessageBoxResult.Yes:
                        foreach (var item in Classes.OrderCart.orderCart)
                        {
                            item.Quantity = 0;
                        }
                        Classes.OrderCart.orderCart.Clear();
                        main.Show();
                        this.Close();
                        break;
                    case MessageBoxResult.No:
                        break;
                    default:
                        main.Show();
                        this.Close();
                        break;
                }
            }
            else
            {
                main.Show();
                this.Close();
            }
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            if (Classes.OrderCart.orderCart.Count() > 0)
            {
                if (TbDiscount.Text.Any(Char.IsLetter) || Convert.ToInt32(TbDiscount.Text) < 0 || Convert.ToInt32(TbDiscount.Text) > 100)
                {
                    MessageBox.Show("Значение скидки неподходит", "Ошибка значения", MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                DB.Sale sale = new DB.Sale();

                sale.StaffId = Classes.SavedUser.SavedEmployee.Id;
                sale.SaleDate = DateTime.Now;
                sale.Discount = Convert.ToDecimal(TbDiscount.Text)/100;
                sale.DeliveryId = (CmbDeliveryType.SelectedItem as DB.DeliveryType).Id;

                Classes.EF.Context.Sale.Add(sale);
                Classes.EF.Context.SaveChanges();

                foreach (var item in Classes.OrderCart.orderCart.Distinct())
                {
                    ProductSale productSale = new ProductSale();
                    productSale.SaleId = sale.Id;
                    productSale.ProductId = item.Id;
                    productSale.Quantity = item.Quantity;

                    Product newValueProd = null;

                    newValueProd = Classes.EF.Context.Product.ToList().Where(i => i.Id == item.Id).FirstOrDefault() as DB.Product;
                    newValueProd.AmountInStock -= item.Quantity;

                    Classes.EF.Context.ProductSale.Add(productSale);
                    Classes.EF.Context.SaveChanges();
                }
                MessageBox.Show("Заказ создан успешно!", "Успех", MessageBoxButton.OK, MessageBoxImage.Information);
                Classes.OrderCart.orderCart.Clear();

                MainMenu main = new MainMenu();
                main.Show();
                this.Close();
            }
        }

        private void BtnDown_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;

            switch (product.MeasureUnit)
            {
                case "единица товара":
                    if (product.Quantity > 1)
                    {
                        product.Quantity--;
                    }

                    SetListValues();
                    break;
                default:
                    if (product.Quantity > (decimal)0.1)
                    {
                        product.Quantity -= (decimal)0.1;
                    }

                    SetListValues();
                    break;
            }
        }

        private void BtnUp_Click(object sender, RoutedEventArgs e)
        {

            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;


            if (product.Quantity < (Classes.EF.Context.Product.ToList().Where(i => i.Id == product.Id).FirstOrDefault() as DB.Product).AmountInStock)
            {
                switch (product.MeasureUnit)
                {
                    case "единица товара":
                        if (product.Quantity < 99)
                        {
                            product.Quantity++;
                        }

                        SetListValues();
                        break;
                    default:
                        if (product.Quantity < 99)
                        {
                            product.Quantity += (decimal)0.1;
                        }

                        SetListValues();
                        break;
                }
            }
            else
            {
                MessageBox.Show("Товара на складе недостаточно", "Ошибка заказа", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void BtnDeleteOrderProduct_Click(object sender, RoutedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var product = button.DataContext as DB.VW_ProductList;

            Classes.OrderCart.orderCart.Remove(product);
            SetListValues();

            product.Quantity = 0;
        }

        private void CmbDeliveryType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DeliveryFee = (int)Classes.EF.Context.DeliveryType.ToList().Where(i => i == (CmbDeliveryType.SelectedItem as DB.DeliveryType)).FirstOrDefault().ExtraFee;
            SetListValues();
            TbDiscount.Text = DeliveryFee.ToString();
        }
    }
}
