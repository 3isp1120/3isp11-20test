﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Magaz.Windows
{
    /// <summary>
    /// Логика взаимодействия для ProductWindow.xaml
    /// </summary>
    public partial class ProductWindow : Window
    {
        public List<DB.VW_ProductList> FilteredList = new List<DB.VW_ProductList>();

        public ProductWindow()
        {
            InitializeComponent();
            SetListValues();
        }

        public void SetListValues()
        {
            LVProductList.ItemsSource = Classes.EF.Context.VW_ProductList.ToList();
        }

        private void LVProductList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var button = sender as Button;
            if (button == null)
            {
                return;
            }
            var service = button.DataContext as DB.VW_ProductList;

            Classes.OrderCart.orderCart.Add(service);
        }

        private void BtnBack_Click(object sender, RoutedEventArgs e)
        {
            MainMenu mainMenu = new MainMenu();
            mainMenu.Show();
            this.Close();
        }

  

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            LVProductList.ItemsSource = Classes.EF.Context.VW_ProductList.ToList().FindAll(i => i.Title.ToLower().Contains(TbFiltr.Text.ToLower()));
        }
    }
}
